from flask import Flask
from apis import blueprint as api
from flask_cors import CORS

app = Flask(__name__, static_folder="static", static_url_path='/static')
app.register_blueprint(api, url_prefix='/api/v1')

CORS(app)

@app.route('/')
def hello_world():
    return 'Wattman Drone Manage System web server APIs. Please go to /api/v1 for more information.'

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
