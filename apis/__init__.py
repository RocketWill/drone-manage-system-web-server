#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

from flask_restplus import Api
from flask import Blueprint
from .system_update import api as system_update_api
from .documents_data import api as docs_data_api

blueprint = Blueprint('api', __name__)

api = Api(blueprint)

api.add_namespace(system_update_api, path="/system-update")
api.add_namespace(docs_data_api, path="/docs")