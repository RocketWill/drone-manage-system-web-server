#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import os
import ntpath
from utils.common import list_files_in_dir
from utils.log import log_error, log_info
from flask_restplus import Resource
from flask_restplus import Namespace
from config import DOCS_DATA_PATH

class DocsDataDto:
    api = Namespace('documents data', description='Documents data related operations.')
    # docs_data_parser = api.parser()
#
api = DocsDataDto.api
# docs_data_parser = DocsDataDto.docs_data_parser

@api.route('/')
class DocsDataList(Resource):
    # @api.expect(docs_data_parser)
    def get(self):
        # args = reocrd_parser.parse_args()
        resp = {'message': 'OK', "code": 200, "result": []}
        docs_data = []
        docs_abs_paths = sorted(list_files_in_dir(DOCS_DATA_PATH, ""))
        try:
            for doc_abs_path in docs_abs_paths:
                doc_data = {}
                doc_data['file_name'] = ntpath.basename(doc_abs_path)
                doc_data['path'] = doc_abs_path.replace(DOCS_DATA_PATH, "/docs")
                docs_data.append(doc_data)
            resp['result'] = docs_data
            log_info("Fetch documents data successfully.")
            return resp, 200
        except Exception as e:
            log_error("Fetch documents data failed, reason: {}".format(str(e)))
            resp = {'message': 'Fail', "code": 500, "result": []}
            return resp, 500

