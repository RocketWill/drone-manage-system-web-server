#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
import os
import pathlib
from flask import request, url_for, abort
from flask import make_response
from utils.validate import validate_update_pack
from utils.common import restart_machine, uncompress, get_file_extension
from utils.log import log_error, log_info
from config import UPDATE_PACK_STORE_PATH, TEMP, CLIENT_PATH, PACK_EXTENSION
from config import CLIENT_VALID_NAME
from flask_restplus import Resource
from core import build_cors_prelight_response
from flask_restplus import Namespace
import werkzeug
from glob import glob
import shutil

class FileUploadDto:
    api = Namespace('system-update', description='System update related operations.')
    file_upload_parser = api.parser()
    file_upload_parser.add_argument('file',
                             type=werkzeug.datastructures.FileStorage,
                             location='files',
                             required=True,
                             help='file')

api = FileUploadDto.api
file_upload_parser = FileUploadDto.file_upload_parser

@api.route('/')
class FileUpload(Resource):
    @api.expect(file_upload_parser)
    def post(self):
        resp = {'message': 'OK', "code": 200}
        args = file_upload_parser.parse_args()
        try:
            # remove files and dir in pack
            clear_dir(UPDATE_PACK_STORE_PATH)
            clear_dir(TEMP)
            extension = get_file_extension(str(args['file'].filename))
            if extension not in PACK_EXTENSION:
                raise ValueError("Unsupported file extension.")
            filename = "update_pack" + extension
            file_des_path = os.path.join(UPDATE_PACK_STORE_PATH, filename)
            args['file'].save(file_des_path)
            uncompress(file_des_path, UPDATE_PACK_STORE_PATH)
            if not validate():
                raise ValueError("The update pack is not official or is damaged.")
            log_info("Upload update pack succesfully.")
            replace_update_pack()
            return resp, 200


        except Exception as e:
            resp = {'message': str(e), "code": 500}
            log_error(str(e))
            return resp, 500

    # def options(self):
    #     return build_cors_prelight_response()

def validate():
    dirs = [dir for dir in glob(os.path.join(UPDATE_PACK_STORE_PATH, "*")) if get_file_extension(dir) not in PACK_EXTENSION]
    return validate_update_pack(dirs)

def clear_dir(dir):
    fds = glob(os.path.join(dir, "*"))
    for path in fds:
        if os.path.isfile(path):
            os.remove(path)
        elif os.path.isdir(path):
            shutil.rmtree(path, ignore_errors=True)
        else:
            os.system("rm -rf {}".format(path))

def move_folder(src, dest):
    try:
        shutil.move(src, dest)
    except Exception as e:
        raise ValueError(e)

def replace_update_pack():
    dirs = glob(os.path.join(UPDATE_PACK_STORE_PATH, "*"))
    dirs.sort(reverse=True)
    update_pack_dir = dirs[0]
    update_packs = glob(os.path.join(UPDATE_PACK_STORE_PATH, "*"))

    # move current version pack to temp folder
    for pack in glob(os.path.join(CLIENT_PATH, "*")):
        print(pack)
        if CLIENT_VALID_NAME in pack and os.path.isdir(pack):
            move_folder(pack, TEMP)

    # move update packs to destination
    for pack in update_packs:
        if CLIENT_VALID_NAME in pack and os.path.isdir(pack):
            move_folder(pack, CLIENT_PATH)

