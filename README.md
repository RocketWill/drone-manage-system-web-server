# 无人机激光测绘 Web Server
## 安装依赖
```bash
pip3 install -r requirements.txt
或
pip install -r requirements.txt
```

## 相关路径配置
需先修改 [config.py](config.py)  
1. `PACK_EXTENSION`: 上传的更新包扩展名列表 (目前支持 `.tar.gz`, `.tar`, `.zip`)
2. `TEMP`: 前一版本包备份路径
3. `UPDATE_PACK_STORE_PATH`: 上传和解压包存放路径
4. `CLIENT_PATH`: 前端项目路径
5. `DOCS_DATA_PATH`: 数据文件存放目录

## Before Start Server
需先将 `DOCS_DATA_PATH` 下的文件夾软连接到本项目下的 `static/docs` 下

## APIs 
### 1. 系统更新（前端）
#### Request
- Method: **POST**
- URL: `http://[address]:5000/api/v1/system-update/`
- Parameters:   

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| file | formData | update pack | Yes | file |
- Curl example:
`curl -X POST "http://[address]:5000/api/v1/system-update/" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "file=@Archive.tar.gz"`

#### Response
- Body
```
Success

{
  "code": 200,
  "message": "OK"
}

Fail
{
  "code": 500,
  "message": "error message"
}
```

### 2. 获取数据文件列表
#### Request
- Method: **GET**
- URL: `http://[address]:5000/api/v1/docs/`
- Parameters:  无

#### Response
- Body
```
Success

{
  "message": "OK",
  "code": 200,
  "result": [
    {
      "name": "file_name.ext",
      "path": "/docs/file_name.ext"
    },
    ...
  ]
}

Fail

{
  "message": "Fail",
  "code": 500,
  "result": []
}
```
通过拼接 `http://[address]:5000/static` 和 `path` 可以获取数据文件