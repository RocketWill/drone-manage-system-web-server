#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

from flask import current_app as app


def log_error(msg):
    app.logger.error(msg)

def log_warning(msg):
    app.logger.warning(msg)

def log_info(msg):
    app.logger.info(msg)
