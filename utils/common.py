#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
from subprocess import call
import os
import tarfile
import zipfile
from pathlib import Path
import glob

def restart_machine():
    # log_info("reboot machine")
    call('echo {} | sudo -S {}'.format("pi", "reboot"), shell=True)

def uncompress(src, dest):
    try:
        if src.endswith('tar.gz'):
            tar = tarfile.open(src, "r:gz")
            tar.extractall(dest)
            tar.close()
        elif src.endswith('tar'):
            tar = tarfile.open(src, "r:")
            tar.extractall()
            tar.close()
        elif src.endswith('zip'):
            with zipfile.ZipFile(src, "r") as zip_ref:
                zip_ref.extractall(dest)
        else:
            raise ValueError("Unsupported extension name to untar/unzip.")
    except Exception as e:
        raise ValueError(str(e))

def change_permissions_recursive(path, mode):
    for root, dirs, files in os.walk(path, topdown=False):
        for dir in [os.path.join(root,d) for d in dirs]:
            os.chmod(dir, mode)
    for file in [os.path.join(root, f) for f in files]:
            os.chmod(file, mode)


def get_file_extension(file_path):
    ext_list = Path(file_path).suffixes
    return "".join(ext_list)


def list_files_in_dir(dir_path, ext="mp4"):
    """
    List file abs path
    :param dir_path: /path/to/directory
    :param ext: file extension
    :return: [abspath, ...]
    """
    try:
        files = glob.glob(os.path.join(dir_path, "*"+ext))
    except:
        files = []
    return files
