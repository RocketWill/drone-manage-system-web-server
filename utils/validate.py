#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
from config import CLIENT_VALID_NAME
def validate_update_pack(packs):
    # detect_pack = False
    # server_pack = False
    client_pack = False

    for pack in packs:
        if CLIENT_VALID_NAME in pack:
            client_pack = True

    if client_pack:
        return True
    return False
